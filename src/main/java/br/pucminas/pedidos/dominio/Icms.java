package br.pucminas.pedidos.dominio;

public class Icms implements Imposto {

	@Override
	public double calcula(Pedido pedido) {
		return pedido.calculaTotal() * 0.18;
	}

}
