package br.pucminas.pedidos.dominio;

public class Iss implements Imposto {

	@Override
	public double calcula(Pedido pedido) {
		return pedido.calculaTotal() * 0.06 + 2.00;
	}

}
