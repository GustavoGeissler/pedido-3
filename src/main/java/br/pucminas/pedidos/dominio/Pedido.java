package br.pucminas.pedidos.dominio;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;


public class Pedido {
	private int numero;
	private Calendar dataDaRealizacao;
	private Collection<ItemDoPedido> itensDoPedido;
	private EstadoDeUmPedido estado;
	
	public Pedido(int numero) {
		if (numero < 0) {
			throw new IllegalArgumentException("O número do pedido deverá ser maior ou igual a zero!");
		}
		this.numero = numero;
		this.dataDaRealizacao = Calendar.getInstance();
		this.itensDoPedido = new ArrayList<ItemDoPedido>();
		this.estado = new EmAprovacao();
	}

	private ItemDoPedido encontraUmItem(Produto produto) {
		ItemDoPedido resultado = null;
		for (ItemDoPedido itemDoPedido : itensDoPedido) {
			if (itemDoPedido.codigoDoProduto() == produto.obtemCodigo()) {
				resultado = itemDoPedido;
				break;
			}
		}
		return resultado;
	}


	public void removeUmItem(Produto produto) {
		ItemDoPedido itemARemover = encontraUmItem(produto);
		if (itemARemover == null) {
			throw new IllegalArgumentException("Item inexistente !");
		}
		itensDoPedido.remove(itemARemover);
	}


	public void incluiItem(Produto produto, double quantidade) {
		this.itensDoPedido.add(new ItemDoPedido(produto, quantidade));
	}


	public double calculaTotal() {
		double total = 0.0;
		for (ItemDoPedido itemDoPedido : itensDoPedido) {
			total += itemDoPedido.calculaTotal();
		}
		return total;
	}

	public double calculaFrete() {
		double total = calculaTotal();

		if (total == 0.0) {
			throw new RuntimeException("Não é possível calcular o frete para um pedido sem itens");
		}

		if (total <= 200.00) {
			return 0.0;
		} else if (total <= 500.00) {
			return 10.0;
		}
		return 12.00;
	}


	public List<ItemDoPedido> retornaItensMaisBaratos() {
		List<ItemDoPedido> resultado = new ArrayList<ItemDoPedido>();
		double maisBarato = 0.0;
		for (ItemDoPedido item : itensDoPedido) {
			double totalDoItem = item.calculaTotal();
			if (maisBarato == 0.0){
				maisBarato = totalDoItem;
			}
			else if (totalDoItem < maisBarato) {
				maisBarato = item.calculaTotal();
			}
		}
		
		for (ItemDoPedido item : itensDoPedido) {
			if (item.calculaTotal() == maisBarato) {
				resultado.add(item);
			}
		}
		return resultado;
	}

	public double calculaImposto(Imposto imposto) {
		return imposto.calcula(this);
	}
	
	@Override
	public String toString() {
		return String.format("Pedido %03d# data=%2$te/%2$tm/%2$tY", this.numero, this.dataDaRealizacao);
	}
	
	protected void mudaEstadoPara(EstadoDeUmPedido novoEstado) {
		this.estado = novoEstado;		
	}
	
	protected void aplicaDescontoNosItens(double desconto) {
		for(ItemDoPedido item : itensDoPedido){
			item.aplica(desconto);
		}
	}
}
