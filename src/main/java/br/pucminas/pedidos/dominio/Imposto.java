/**
 * 
 */
package br.pucminas.pedidos.dominio;


public interface Imposto {
	double calcula(Pedido pedido);
}
