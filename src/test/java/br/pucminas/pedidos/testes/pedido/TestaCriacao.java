package br.pucminas.pedidos.testes.pedido;

import org.junit.Test;

import br.pucminas.pedidos.dominio.Pedido;

public class TestaCriacao {
	@Test(expected = IllegalArgumentException.class)
	public void criaUmPedidoComNumeroNegativo() {
		@SuppressWarnings("unused")
		Pedido pedido = new Pedido(-1);
	}
}
