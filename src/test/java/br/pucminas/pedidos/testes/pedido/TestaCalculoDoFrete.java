package br.pucminas.pedidos.testes.pedido;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.pucminas.pedidos.dominio.Pedido;
import br.pucminas.pedidos.dominio.Produto;

public class TestaCalculoDoFrete {
	private Pedido pedido;
	private static Produto caneta;
	private static Produto lapis;

	@BeforeClass
	public static void configuraClasseDeTeste() {
		caneta = new Produto(1, "Caneta", 1.50);
		lapis = new Produto(2, "Lapis", 1.00);
	}

	@Before
	public void configurTeste() {
		pedido = new Pedido(100);
	}

	@Test(expected = RuntimeException.class)
	public void calculaOFreteParaUmPedidoSemItens() {
		pedido.calculaFrete();
	}

	@Test
	public void calculaOFreteParaUmPedidoDe150Reais() {
		pedido.incluiItem(caneta, 80);
		pedido.incluiItem(lapis, 30);

		assertEquals(0.0, pedido.calculaFrete(), 0.01);
	}

	@Test
	public void calculaOFreteParaUmPedidoDe200Reais() {
		pedido.incluiItem(caneta, 100);
		pedido.incluiItem(lapis, 50);

		assertEquals(0.0, pedido.calculaFrete(), 0.01);
	}

	@Test
	public void calculaOFreteParaUmPedidoDe300Reais() {
		pedido.incluiItem(caneta, 180);
		pedido.incluiItem(lapis, 30);

		assertEquals(10.0, pedido.calculaFrete(), 0.01);
	}

	@Test
	public void calculaOFreteParaUmPedidoDe600Reais() {
		pedido.incluiItem(caneta, 400);

		assertEquals(12.0, pedido.calculaFrete(), 0.01);
	}

}
