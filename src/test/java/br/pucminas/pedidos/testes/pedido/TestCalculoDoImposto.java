package br.pucminas.pedidos.testes.pedido;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.pucminas.pedidos.dominio.Icms;
import br.pucminas.pedidos.dominio.Ipi;
import br.pucminas.pedidos.dominio.Iss;
import br.pucminas.pedidos.dominio.Pedido;
import br.pucminas.pedidos.dominio.Produto;

public class TestCalculoDoImposto {

	private static Produto caneta;
	private static Produto lapis;

	private Pedido pedido;

	@BeforeClass
	public static void configuraTodosOsTestes() {
		caneta = new Produto(1, "Caneta", 1.00);
		lapis = new Produto(1, "Lápis", 2.00);
	}

	@Before
	public void configuraUmTeste() {
		pedido = new Pedido(123678);
		pedido.incluiItem(caneta, 100);
	}

	@Test
	public void oCalculoDoImpostoDeveraRetornarOICMSparaUmPedido() {
		assertEquals(pedido.calculaTotal() * 0.18, pedido.calculaImposto(new Icms()), 0.01);
	}

	@Test
	public void oCalculoDoImpostoDeveraRetornarOISSparaUmPedido() {
		assertEquals(pedido.calculaTotal() * 0.06 + 2.00, pedido.calculaImposto(new Iss()), 0.01);
	}

	@Test
	public void oCalculoDoImpostoDeveraRetornarOIPIparaUmPedidoComValorMenorDoQue1000() {
		assertEquals(pedido.calculaTotal() * 0.07, pedido.calculaImposto(new Ipi()), 0.01);
	}

	@Test
	public void oCalculoDoImpostoDeveraRetornarOIPIparaUmPedidoComValorMenorOuIgualA3000() {
		pedido.incluiItem(lapis, 1000);
		assertEquals(pedido.calculaTotal() * 0.08, pedido.calculaImposto(new Ipi()), 0.01);
	}

	@Test
	public void oCalculoDoImpostoDeveraRetornarOIPIparaUmPedidoComValorMaiorDoQue3000() {
		pedido.incluiItem(lapis, 3000);
		assertEquals(pedido.calculaTotal() * 0.09, pedido.calculaImposto(new Ipi()), 0.01);
	}

}
