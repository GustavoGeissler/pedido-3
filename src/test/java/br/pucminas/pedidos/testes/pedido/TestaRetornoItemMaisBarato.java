package br.pucminas.pedidos.testes.pedido;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.pucminas.pedidos.dominio.ItemDoPedido;
import br.pucminas.pedidos.dominio.Pedido;
import br.pucminas.pedidos.dominio.Produto;

public class TestaRetornoItemMaisBarato {

	private Pedido pedido;
	private static Produto lapis;
	private static Produto caneta;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		lapis = new Produto(1, "Lapis", 1.50);
		caneta = new Produto(2, "Caneta", 2.00);
	}

	@Before
	public void setUp() throws Exception {
		pedido = new Pedido(100);
	}

	@Test
	public void retornaItemMaisBarato_RetornaListaVazia_ParaUmPedidoVazio() {
		// Arranjo

		// Acão
		List<ItemDoPedido> resultado = pedido.retornaItensMaisBaratos();
		// Asserção
		assertEquals(0, resultado.size());

	}

	@Test
	public void retornaItemMaisBarato_RetornaListaComUmItem_ParaUmPedido() {
		// Arranjo
		pedido.incluiItem(lapis, 2);
		pedido.incluiItem(caneta, 1);
		List<ItemDoPedido> esperado = Arrays.asList(new ItemDoPedido(caneta, 1));

		// Ação
		List<ItemDoPedido> resultado = pedido.retornaItensMaisBaratos();

		// Asserção

		assertList(esperado, resultado);
	}

	@Test
	public void retornaItemMaisBarato_RetornaListaComDoisItens_ParaUmPedido() {
		// Arranjo
		pedido.incluiItem(lapis, 4);
		pedido.incluiItem(caneta, 3);
		List<ItemDoPedido> esperado = Arrays.asList(new ItemDoPedido(lapis, 4), new ItemDoPedido(caneta, 3));

		// Ação
		List<ItemDoPedido> resultado = pedido.retornaItensMaisBaratos();

		// Asserção

		assertList(esperado, resultado);
	}

	private void assertList(List<ItemDoPedido> esperado, List<ItemDoPedido> resultado) {

		Collection<ItemDoPedido> e = new ArrayList(esperado);
		Collection<ItemDoPedido> r = new ArrayList(resultado);
		assertEquals(false, e.retainAll(r));

	}

}
