package br.pucminas.pedidos.testes.pedido;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.pucminas.pedidos.dominio.Pedido;
import br.pucminas.pedidos.dominio.Produto;

public class TestaRemocaoDeItens {

	private Pedido pedido;
	private static Produto lapis;
	private static Produto caneta;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		lapis = new Produto(1, "Lapis", 1.50);
		caneta = new Produto(2, "Caneta", 2.00);
	}

	@Before
	public void setUp() throws Exception {
		pedido = new Pedido(100);
	}

	@Test
	public void removeItem_RemoveraUmItem_ParaUmItemExistente() {
		//Arranjo
		double totalEsperado = 2 * 1.50;

		// Acão
		pedido.incluiItem(lapis, 2);
		pedido.incluiItem(caneta, 1);
		pedido.removeUmItem(caneta);
		
		// Asserção
		assertEquals(totalEsperado, pedido.calculaTotal(), 0.01);

	}

	@Test(expected = IllegalArgumentException.class)
	public void removeItem_LancaraUmaExcecao_ParaUmItemInexistente() {
		//Arranjo
		
		//Asserção
		pedido.incluiItem(lapis, 2);
		pedido.removeUmItem(caneta);
		
		// Ação
	}

	@Test(expected = IllegalArgumentException.class)
	public void removeItem_LancaraUmaExcecao_ParaUmPedidoVazio() {
		//Arranjo
		//Asserção
		pedido.removeUmItem(caneta);
		// Ação
	}

}
